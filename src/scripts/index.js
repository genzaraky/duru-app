import '../styles/index.scss';
import menuicon from '../images/icons/menu.svg';
require('angular');
require('angular-animate');
require('angular-route');
require('angular-sanitize');
require('angular-touch');


var firebaseConfig = {
    apiKey: "AIzaSyDRoAZV_Q6LNmt5ssPWXGUFjVMdjdtOiD4",
    authDomain: "duru-c5406.firebaseapp.com",
    databaseURL: "https://duru-c5406.firebaseio.com",
    projectId: "duru-c5406",
    storageBucket: "duru-c5406.appspot.com",
    messagingSenderId: "669318291827",
    appId: "1:669318291827:web:cc5634efab532546386506",
    measurementId: "G-DV8TC4DMNJ"
  };
 
    firebase.initializeApp(firebaseConfig);
    var duruDb=firebase.firestore();
    var app = angular.module("duru", 
    [
        "ngRoute",
        "ngAnimate",
        "ngSanitize",
        "ngTouch"
    
    ]);


/***
**** ROUTES
***/

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl: 'src/store/home.html',
        controller: 'store.home.ctrl'          
    })
    .when("/single/:ref", {
        templateUrl: 'src/store/single.html',
        controller: 'store.post.ctrl'          
    })
    .when("/single", {
        templateUrl: 'src/store/singles.html',
        controller: 'store.singles.ctrl'          
    })
    .when("/album/:ref", {
        templateUrl: 'src/store/album.html',
        controller: 'store.post.ctrl'          
    })
    .when("/album", {
        templateUrl: 'src/store/albums.html',
        controller: 'store.albums.ctrl'      
    })
    .when("/search", {
        templateUrl: 'src/store/search.html',
        controller: 'store.search.ctrl'      
    })
    .when("/cart", {
        templateUrl: 'src/store/cart.html',
        controller: 'store.cart.ctrl'      
    })
    .when("/checkout", {
        templateUrl: 'src/store/checkout.html',
        controller: 'store.checkout.ctrl'      
    })
    .when("/checkout/momo", {
        templateUrl: 'src/store/checkout.momo.html',
        controller: 'store.checkout.momo.ctrl'      
    })
    .when("/mymusics", {
        templateUrl: 'src/store/mymusics.html',
        controller: 'store.mymusics.ctrl'      
    })
    .when("/mymusics/:ref", {
        templateUrl: 'src/store/mymusics.post.html',
        controller: 'store.mymusics.post.ctrl'      
    })
    .otherwise({ redirectTo: '/404'});
  
    $locationProvider.html5Mode(true);
  });  

/***
**** CONTROLLERS
***/
app.controller("ctrl",function($scope,$http,$rootScope,$location){
    console.log('Duru is working');
    
});

app.controller("store.home.ctrl",["$http","$rootScope","$scope","$location",function($http,$rootScope,$scope,$location){
    console.log('Duru is working');
    $scope.menu={};
    $scope.singles =[];
    $scope.albums =[];
    var dbPosts = duruDb.collection("post");
    
  
    dbPosts.where("type","==","single").get().then((querySnapshot) => {        
        querySnapshot.forEach((doc) => {            
            $scope.$apply(function(){                
                $scope.singles.push(doc.data());
                $scope.singles[$scope.singles.length-1].id = doc.id;
            });
            
        });
    });
    dbPosts.where("type","==","album").get().then((querySnapshot) => {        
        querySnapshot.forEach((doc) => {
            console.log(doc.data());
            $scope.$apply(function(){
                $scope.albums.push(doc.data());
                $scope.albums[$scope.albums.length-1].id = doc.id;
            });
            
        });
    });
    /*for(var i=1;i<9;i++){
        $scope.elements.push(
            {
                "name":"Sexy Mama",
                "slug":"sexy-mama",
                "artist":"Wizkid",
                "artwork":"src/images/post/pic"+i+".png",
            }
            );
    }*/
    /*for(var i=8;i>0;i--){
        $scope.albums.push(
            {
                "name":"Sexy Mama",
                "slug":"sexy-mama",
                "artist":"Wizkid",
                "artwork":"src/images/post/pic"+i+".png",
            }
            );
    }*/
    $scope.menu.close=function(){
        $scope.menu.opened=false;
    }
    $scope.menu.open=function(){
        $scope.menu.opened=true;
    }
}]);
app.controller("store.post.ctrl",["$http","$routeParams","$scope","$location","Nav",function($http,$routeParams,$scope,$location,Nav){
    $scope.element={};
    $scope.Nav=Nav;
    var dbPosts = duruDb.collection("post");
    dbPosts.doc($routeParams.ref).get().then((doc) => {        
         if (doc.exists) {
            console.log("Document data:", doc.data());
            $scope.$apply(function(){
                $scope.element = doc.data();
                $scope.element.id = doc.id;
            });
            
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    });
   

    
}]);
app.controller("store.albums.ctrl",["$http","$routeParams","$scope","$location","Nav",function($http,$routeParams,$scope,$location,Nav){
    console.log('Duru single page');
    $scope.Nav=Nav;
    $scope.elements =[];
    var dbPosts = duruDb.collection("post");
    
  
    dbPosts.where("type","==","album").get().then((querySnapshot) => {        
        querySnapshot.forEach((doc) => {
            $scope.$apply(function(){

                $scope.elements.push(doc.data());
                $scope.elements[$scope.elements.length-1].id = doc.id;
            });
            
        });
    });
    
    $scope.dumies=function(i){
        
        console.log($scope.elements);
        for(var y=1;y<=i;y++){
            var index=$scope.elements.length+1; 
            $scope.elements.push(
                {   
                    "id":index,
                    "name":"Sexy Mama",
                    "slug":"sexy-mama",
                    "artist":"Wizkid",
                    "artwork":"src/images/post/pic"+y+".png",
                }
                );
        }
    }
    $scope.loadMore=function(){
        //$scope.dumies(4);
    }
    
}]);
app.controller("store.singles.ctrl",["$http","$routeParams","$scope","$location","Nav",function($http,$routeParams,$scope,$location,Nav){
    
    $scope.Nav=Nav;
    $scope.elements =[];
    var dbPosts = duruDb.collection("post");
    dbPosts.where("type","==","single").get().then((querySnapshot) => {        
        querySnapshot.forEach((doc) => {
            $scope.$apply(function(){

                $scope.elements.push(doc.data());
                $scope.elements[$scope.elements.length-1].id = doc.id;
            });
            
        });
    });
    
    $scope.dumies=function(i){
        
        console.log($scope.elements);
        for(var y=1;y<=i;y++){
            var index=$scope.elements.length+1; 
            $scope.elements.push(
                {   
                    "id":index,
                    "name":"Sexy Mama",
                    "slug":"sexy-mama",
                    "artist":"Wizkid",
                    "artwork":"src/images/post/pic"+y+".png",
                }
                );
        }
    }
    $scope.loadMore=function(){
        //$scope.dumies(4);
    }
    
}]);
/***
**** SERVICES
***/
app.service('Nav',function(){
    this.back= function(){
        
        console.log('back');
    }
});